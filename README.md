irl toolkit
===========

Originally created for Anaheim Girl's Love Story, **irl** will hopefully be a toolkit for extraction, insertion, repacking, and general interaction with the files of the LiveMaker 3 engine.

But as this is currently under active development, it doesn't do all of that yet.

Big thanks to Nagato for reversing almost all of LiveMaker 3's script format in 2 days!

Currently falls under the legal auspices of the GPLv3.
